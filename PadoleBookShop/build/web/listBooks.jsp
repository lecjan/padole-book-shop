<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.*" %>
<%@ page import="DTO.User" %>
<%@ page import="DTO.Book" %>
<%@ page import="java.util.ArrayList" %> 

<head>
    <title>Book Store PaDoLe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/Site.css" type="text/css">
    <link rel="stylesheet" href="CSS/jquery.dataTables.css" type="text/css">
    <script type="text/javascript" src="JS/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="JS/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="JS/PadoleScripts.js"></script>
</head>

<body>
    <div class="page">
        
        <!-- ************************************************************* -->
        <div class="header">
           
            <div class="title">
                <h1>Book Store PADOLE</h1>                
            </div>
            
            <div id="login_section">
                <div id="login_buttons">
                    <form id="login_logout_form" method="post" name="form3" action='ActionServlet'>
                        <input type="hidden" name="action" value="Logout" id="submit_action"/>
                        <input type="Submit" value="Logout" onclick=""/>                
                    </form>   
                </div>
            </div>            
            
            <!-- menu container -->
            <div class="clear hideSkiplink">                 
            </div>
            
        </div>     
        <!-- ************************************************************* -->
        <div class="main">
            <div class="main_left">

<table id="example" class="display" cellspacing="0" width="100%" style="font-size: .8em">
        <thead>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th>Genre</th>
                <th>Price</th>
                <th>Qty</th>
            </tr>
        </thead>
 
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Author</th>
                <th>Genre</th>
                <th>Price</th>
                <th>Qty</th>
            </tr>
        </tfoot>
 
        <tbody>            
            <%
                ArrayList<Book> bookList = new ArrayList<>();
                bookList = (ArrayList) request.getSession().getAttribute("books");
                for (Book b: bookList){ 
                    if (b.getQty() != 0)
                {%>
                 
                    <tr onclick="orderBook(<%=b.getBookId() %>,'<%=b.getTitle()%>','<%=b.getAuthor()%>','<%=b.getGenre() %>',<%=b.getPrice() %>)">
                        <td data-search="<%=b.getBookId() %>"><%=b.getBookId() %></td>
                        <td data-search="<%=b.getTitle() %>"><%=b.getTitle()%></td>
                        <td data-search="<%=b.getAuthor()%>"><%=b.getAuthor()%></td>
                        <td data-search="<%=b.getGenre() %>"><%=b.getGenre() %></td>
                        <td data-search="<%=b.getPrice() %>"><%=b.getPrice() %></td>
                        <td data-search="<%=b.getQty() %>"><%=b.getQty() %></td>
                    </tr>                    
                <% }
                } %>
        </tbody>
    </table>              
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#example').dataTable();
                    } );
                </script>                    
                    
            </div>

            <div class="main_right">
                 <h2>Navigation</h2>
                 <h1>Where would you like to go.....</h1>
                <br>
                <div><a href="userAccount.jsp">View/Edit Your Account Details</a></div>
                
                <br>
                <div><a href="ActionServlet?action=viewOrders">View Your Orders</a></div>
                
                <h2>Chapter Three</h2>
                <h1>"Select The One Book You Want.."</h1>  
              
               
               <div id="single_order" style="text-align: right">
                    <form id="single_order_form" method="post" name="form5" action="ActionServlet">
                        <div id="order_Text">
                            ID:<input type="text" name="bookId" value="a" id="bookIdText"readonly/><br/>
                            Title:<input type="text"name="title" value="b" id="titleText"readonly/><br/> 
                            Author:<input type="text" name="author" value="c" id="authorText"readonly/><br/> 
                            Genre:<input type="text" name="genre" value="d" id="genreText"readonly/><br/> 
                            Price:<input type="text" name="price" value="e" id="priceText"readonly/><br/> 
                        </div>
                        <input type="hidden" name="action" value="placeOrder" id="submit_action"/>
                        <input type="Submit" value="Buy" onclick=""/> 
                        <input type="button" value="Cancel" onclick="orderBookCancel()"/> 
                    </form>
                    
                </div>
            </div>        
        </div>
        
        <!-- ************************************************************* -->        
        <div class="footer">   
            <center>
                <br/><br/><br/>
            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>            
            </center>
        </div>
        
    </div>
</body>


