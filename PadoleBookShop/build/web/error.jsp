<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
   
<head>
    <title>PADOLE: Login Failure</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/Site.css" type="text/css">
    <link rel="stylesheet" href="CSS/jquery.dataTables.css" type="text/css">
    <script type="text/javascript" src="JS/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="JS/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="JS/PadoleScripts.js"></script>
</head>

<body>
    <div class="page">
        
        <!-- ************************************************************* -->
        <div class="header">
           
            <div class="title">
                <h1>Book Store PADOLE</h1>                
            </div>
            
            <div id="login_section">
                <div id="login_buttons">
                </div>
            </div>           
            
            <!-- menu container -->
            <div class="clear hideSkiplink">                 
            </div>
            
        </div>     
        <!-- ************************************************************* -->
        <div class="main">
            <div class="main_left">     
            </div>

            <div class="main_right">
                <h2>Appendix</h2>
                <br/>
                <h1>"Error processing data...."</h1>
                <br/>
                <form id="error_form" method="post" action="userAccount.jsp">
                    <input type="submit" value="Return to User Account" >    
                </form>
                
            <div id="login_display">
            </div>   
                
            </div>        
        </div>
        
        <!-- ************************************************************* -->        
        <div class="footer">   
            <center>
                <br/><br/><br/>
            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>            
            </center>
        </div>
        
    </div>
</body>




