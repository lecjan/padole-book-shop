<%@page import="DTO.User"%>
<html>    
<head>
    <title>Book Store PaDoLe | Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/Site.css" type="text/css">
    <link rel="stylesheet" href="CSS/jquery.dataTables.css" type="text/css">
    <script type="text/javascript" src="JS/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="JS/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="JS/PadoleScripts.js"></script>
</head>

<body>
    <div class="page">
        
        <!-- ************************************************************* -->
        <div class="header">
           
            <div class="title">
                <h1>Book Store PADOLE</h1>                
            </div>
            
            <div id="login_section">
                <div id="login_buttons">
                    Welcome... 
                    <% User usr = (User) (request.getSession().getAttribute("user")); %>
                    <% out.print(usr.getfName()+" "+usr.getsName()); %>  
                    <form id="login_logout_form" method="post" name="form3" action='ActionServlet'>
                        <input type="hidden" name="action" value="Logout" id="submit_action"/>
                        <input type="Submit" value="Logout" onclick=""/>                
                    </form>   
                </div>                
            </div>            
            
            <!-- menu container -->
            <div class="clear hideSkiplink"> 
            </div>
            
        </div>     
        <!-- ************************************************************* -->
        <div class="main">
            <div class="main_left">
                        <h2>Account</h2>
                <h1>Your details..</h1>
                
                
            <div id="login_display">
                <form id="edit_form" method="post" name="form9" action="ActionServlet">
                    <br/>
                    
                    <label>First name :</label>
                                  
                    <input type="text" required pattern="^[a-zA-Z' ]{2,50}$" name="efName" id="fNameDetails" title='Minimum 2 characters (letters, single quote and space)' value = "<% out.print(usr.getfName()); %>" readonly/>
                    <br/>
                    
                    <label>Second name :</label>
                                 
                    <input type="text" required pattern="^[a-zA-Z' ]{2,50}$" name="esName" id="sNameDetails" title='Minimum 2 characters (letters,single quote and space)' value = "<% out.print(usr.getsName()); %>" readonly/>
                    <br/>
                    
                    <label>Phone :</label>
                                   
                    <input type="tel" required pattern="^[0-9]{6,50}$" name="ephone" id="phoneDetails" title='Minimum 6 digits' value = "<% out.print(usr.getPhone()); %>" readonly/>
                    <br/>
                    
                    <label>Address line 1 :</label>
                                 
                    <input type="text" required pattern="^[0-9a-zA-Z'., ]{3,50}$" name="eaddress1" id="address1Details" title='Minimum 3 characters (letters, digits, single quote, dot and space)' value = "<% out.print(usr.getAddress1()); %>" readonly/>
                    <br/>      
                    
                    <label>Address line 2 :</label>
                                  
                    <input type="text" name="eaddress2" id="address2Details" title='Minimum 3 characters (letters, digits, single quote, dot and space)' value = "<% out.print(usr.getAddress2()); %>" readonly/>
                    <br/> 
                    
                    <label>Address City :</label>
                                   
                    <input type="text" required pattern="^[a-zA-Z' ]{3,50}$" name="ecity" id="cityDetails" title='Minimum 2 characters (letters, single quote and space)' value = "<% out.print(usr.getCity()); %>" readonly/>
                    <br/> 
                    
                    <label>Address Country :</label>
                                 
                    <input type="text" required pattern="^[a-zA-Z' ]{1,50}$" name="ecountry" id="countryDetails" title='Minimum 2 characters (letters, single quote and space)' value = "<% out.print(usr.getCountry()); %>" readonly/>
                    <br/><br/>                       

                    
                    
                    
                    <input type="button" value="Edit" id="edit_details" onclick="editDetails()"/>
                    <input type="hidden" name="action" value="editUser"/>
                    <input type="submit" value="Save" id="save" style="visibility: collapse"/>
                    <br/><br/>
                    
                    <!-- ERROR MESSAGE FOR JAVASCRIPT -->
                    <div id="error"></div>
                    
                </form>
            </div> 
                <script>registrationDisplay();</script>
            </div>

            <div class="main_right">
                <h2>Navigation</h2>
                <h1>Where would you like to go.....</h1>
                <br>
                <div><a href="ActionServlet?action=viewBooks">View/Order Books</a></div>
                <br>
                <div><a href="ActionServlet?action=viewOrders">View Your Orders</a></div>
            </div>        
        </div>
        
        <!-- ************************************************************* -->        
        <div class="footer">   
            <center>
                <br/><br/><br/>
            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>   
            </center>
        </div>
        
    </div>
</body>
</html>


