package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import DTO.User;

public final class userAccount_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("<html>    \n");
      out.write("<head>\n");
      out.write("    <title>Book Store PaDoLe | Registration</title>\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/Site.css\" type=\"text/css\">\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/jquery.dataTables.css\" type=\"text/css\">\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery-1.11.1.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery.dataTables.min.js\"></script>\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/PadoleScripts.js\"></script>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("<body>\n");
      out.write("    <div class=\"page\">\n");
      out.write("        \n");
      out.write("        <!-- ************************************************************* -->\n");
      out.write("        <div class=\"header\">\n");
      out.write("           \n");
      out.write("            <div class=\"title\">\n");
      out.write("                <h1>Book Store PADOLE</h1>                \n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("            <div id=\"login_section\">\n");
      out.write("                <div id=\"login_buttons\">\n");
      out.write("                    Welcome... \n");
      out.write("                    ");
 User usr = (User) (request.getSession().getAttribute("user")); 
      out.write("\n");
      out.write("                    ");
 out.print(usr.getfName()+" "+usr.getsName()); 
      out.write("  \n");
      out.write("                    <form id=\"login_logout_form\" method=\"post\" name=\"form3\" action='ActionServlet'>\n");
      out.write("                        <input type=\"hidden\" name=\"action\" value=\"Logout\" id=\"submit_action\"/>\n");
      out.write("                        <input type=\"Submit\" value=\"Logout\" onclick=\"\"/>                \n");
      out.write("                    </form>   \n");
      out.write("                </div>                \n");
      out.write("            </div>            \n");
      out.write("            \n");
      out.write("            <!-- menu container -->\n");
      out.write("            <div class=\"clear hideSkiplink\"> \n");
      out.write("            </div>\n");
      out.write("            \n");
      out.write("        </div>     \n");
      out.write("        <!-- ************************************************************* -->\n");
      out.write("        <div class=\"main\">\n");
      out.write("            <div class=\"main_left\">\n");
      out.write("                        <h2>Account</h2>\n");
      out.write("                <h1>Your details..</h1>\n");
      out.write("                \n");
      out.write("                \n");
      out.write("            <div id=\"login_display\">\n");
      out.write("                <form id=\"edit_form\" method=\"post\" name=\"form9\" action=\"ActionServlet\">\n");
      out.write("                    <br/>\n");
      out.write("                    \n");
      out.write("                    <label>First name :</label>\n");
      out.write("                                  \n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{2,50}$\" name=\"efName\" id=\"fNameDetails\" title='Minimum 2 characters (letters, single quote and space)' value = \"");
 out.print(usr.getfName()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/>\n");
      out.write("                    \n");
      out.write("                    <label>Second name :</label>\n");
      out.write("                                 \n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{2,50}$\" name=\"esName\" id=\"sNameDetails\" title='Minimum 2 characters (letters,single quote and space)' value = \"");
 out.print(usr.getsName()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/>\n");
      out.write("                    \n");
      out.write("                    <label>Phone :</label>\n");
      out.write("                                   \n");
      out.write("                    <input type=\"tel\" required pattern=\"^[0-9]{6,50}$\" name=\"ephone\" id=\"phoneDetails\" title='Minimum 6 digits' value = \"");
 out.print(usr.getPhone()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/>\n");
      out.write("                    \n");
      out.write("                    <label>Address line 1 :</label>\n");
      out.write("                                 \n");
      out.write("                    <input type=\"text\" required pattern=\"^[0-9a-zA-Z'., ]{3,50}$\" name=\"eaddress1\" id=\"address1Details\" title='Minimum 3 characters (letters, digits, single quote, dot and space)' value = \"");
 out.print(usr.getAddress1()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/>      \n");
      out.write("                    \n");
      out.write("                    <label>Address line 2 :</label>\n");
      out.write("                                  \n");
      out.write("                    <input type=\"text\" name=\"eaddress2\" id=\"address2Details\" title='Minimum 3 characters (letters, digits, single quote, dot and space)' value = \"");
 out.print(usr.getAddress2()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/> \n");
      out.write("                    \n");
      out.write("                    <label>Address City :</label>\n");
      out.write("                                   \n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{3,50}$\" name=\"ecity\" id=\"cityDetails\" title='Minimum 2 characters (letters, single quote and space)' value = \"");
 out.print(usr.getCity()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/> \n");
      out.write("                    \n");
      out.write("                    <label>Address Country :</label>\n");
      out.write("                                 \n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{1,50}$\" name=\"ecountry\" id=\"countryDetails\" title='Minimum 2 characters (letters, single quote and space)' value = \"");
 out.print(usr.getCountry()); 
      out.write("\" readonly/>\n");
      out.write("                    <br/><br/>                       \n");
      out.write("\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    <input type=\"button\" value=\"Edit\" id=\"edit_details\" onclick=\"editDetails()\"/>\n");
      out.write("                    <input type=\"hidden\" name=\"action\" value=\"editUser\"/>\n");
      out.write("                    <input type=\"submit\" value=\"Save\" id=\"save\" style=\"visibility: collapse\"/>\n");
      out.write("                    <br/><br/>\n");
      out.write("                    \n");
      out.write("                    <!-- ERROR MESSAGE FOR JAVASCRIPT -->\n");
      out.write("                    <div id=\"error\"></div>\n");
      out.write("                    \n");
      out.write("                </form>\n");
      out.write("            </div> \n");
      out.write("                <script>registrationDisplay();</script>\n");
      out.write("            </div>\n");
      out.write("\n");
      out.write("            <div class=\"main_right\">\n");
      out.write("                <h2>Navigation</h2>\n");
      out.write("                <h1>Where would you like to go.....</h1>\n");
      out.write("                <br>\n");
      out.write("                <div><a href=\"ActionServlet?action=viewBooks\">View/Order Books</a></div>\n");
      out.write("                <br>\n");
      out.write("                <div><a href=\"ActionServlet?action=viewOrders\">View Your Orders</a></div>\n");
      out.write("            </div>        \n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("        <!-- ************************************************************* -->        \n");
      out.write("        <div class=\"footer\">   \n");
      out.write("            <center>\n");
      out.write("                <br/><br/><br/>\n");
      out.write("            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>   \n");
      out.write("            </center>\n");
      out.write("        </div>\n");
      out.write("        \n");
      out.write("    </div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
      out.write("\n");
      out.write("\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
