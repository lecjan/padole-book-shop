package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import DTO.User;

public final class loginRegistration_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<html>    \r\n");
      out.write("<head>\r\n");
      out.write("    <title>Book Store PaDoLe | Registration</title>\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/Site.css\" type=\"text/css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/jquery.dataTables.css\" type=\"text/css\">\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery-1.11.1.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery.dataTables.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/PadoleScripts.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("    <div class=\"page\">\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"header\">\r\n");
      out.write("           \r\n");
      out.write("            <div class=\"title\">\r\n");
      out.write("                <h1>Book Store PADOLE</h1>                \r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("            <div id=\"login_section\">\r\n");
      out.write("                <div id=\"login_buttons\">\r\n");
      out.write("                    Welcome... \r\n");
      out.write("                    ");
 String username = (String) (request.getSession().getAttribute("newusername")); 
      out.write("\r\n");
      out.write("                    ");
 out.print(username); 
      out.write("  \r\n");
      out.write("                    <form id=\"login_logout_form\" method=\"post\" name=\"form3\" action='ActionServlet'>\r\n");
      out.write("                        <input type=\"hidden\" name=\"action\" value=\"Logout\" id=\"submit_action\"/>\r\n");
      out.write("                        <input type=\"Submit\" value=\"Logout\" onclick=\"\"/>                \r\n");
      out.write("                    </form>   \r\n");
      out.write("                </div>                \r\n");
      out.write("            </div>            \r\n");
      out.write("            \r\n");
      out.write("            <!-- menu container -->\r\n");
      out.write("            <div class=\"clear hideSkiplink\"> \r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("        </div>     \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"main\">\r\n");
      out.write("            <div class=\"main_left\">\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"main_right\">\r\n");
      out.write("                <h2>Chapter Three</h2>\r\n");
      out.write("                <h1>\"To claim your Key<br/>we need your details..\"</h1>\r\n");
      out.write("                \r\n");
      out.write("                \r\n");
      out.write("            <div id=\"login_display\">\r\n");
      out.write("                <form id=\"registration_form\" method=\"post\" name=\"form4\" action=\"ActionServlet\">\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <!-- fName -->\r\n");
      out.write("                    <label>First name :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{2,50}$\" name=\"fName\" id=\"fNameText\" title='Minimum 2 characters (letters, single quote and space)'/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <!-- sName -->\r\n");
      out.write("                    <label>Second name :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{2,50}$\" name=\"sName\" id=\"sNameText\" title='Minimum 2 characters (letters,single quote and space)'/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <!-- phone -->\r\n");
      out.write("                    <label>Phone :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"tel\" required pattern=\"^[0-9]{6,50}$\" name=\"phone\" id=\"phoneText\" title='Minimum 6 digits'/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <!-- address1 -->\r\n");
      out.write("                    <label>Address line 1 :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"^[0-9a-zA-Z'., ]{3,50}$\" name=\"address1\" id=\"address1Text\" title='Minimum 3 characters (letters, digits, single quote, dot and space)'/>\r\n");
      out.write("                    <br/>      \r\n");
      out.write("                    <!-- address2 -->\r\n");
      out.write("                    <label>Address line 2 :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" name=\"address2\" id=\"address2Text\" title='Minimum 3 characters (letters, digits, single quote, dot and space)'/>\r\n");
      out.write("                    <br/> \r\n");
      out.write("                    <!-- city -->\r\n");
      out.write("                    <label>Address City :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{3,50}$\" name=\"city\" id=\"cityText\" title='Minimum 2 characters (letters, single quote and space)'/>\r\n");
      out.write("                    <br/> \r\n");
      out.write("                    <!-- country -->\r\n");
      out.write("                    <label>Address Country :</label>\r\n");
      out.write("                    <!-- pattern for validation\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"^[a-zA-Z' ]{1,50}$\" name=\"country\" id=\"countryText\" title='Minimum 2 characters (letters, single quote and space)'/>\r\n");
      out.write("                    <br/><br/>                       \r\n");
      out.write("\r\n");
      out.write("                    <!-- BUTTONS -->\r\n");
      out.write("                    <!-- submit value changes according to value set by login_buttons_form,\r\n");
      out.write("                         and this is to use for login_form to display proper submit type,\r\n");
      out.write("                         also hidden input takes same value as an action to use by servlet -->\r\n");
      out.write("                    \r\n");
      out.write("                    <input type=\"hidden\" name=\"action\" value=\"addUser\" id=\"submit_action\"/>\r\n");
      out.write("                    <input type=\"submit\" value=\"Save\" id=\"submit_login\" onclick=\"validateRegistration()\"/>\r\n");
      out.write("                    <input type=\"button\" value=\"Cancel\" id=\"submit_cancel\" onclick=\"registrationCancel()\"/>\r\n");
      out.write("                    <br/><br/>\r\n");
      out.write("                    \r\n");
      out.write("                    <!-- ERROR MESSAGE FOR JAVASCRIPT -->\r\n");
      out.write("                    <div id=\"error\"></div>\r\n");
      out.write("                    \r\n");
      out.write("                </form>\r\n");
      out.write("            </div> \r\n");
      out.write("                <script>registrationDisplay();</script>\r\n");
      out.write("            </div>        \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->        \r\n");
      out.write("        <div class=\"footer\">   \r\n");
      out.write("            <center>\r\n");
      out.write("                <br/><br/><br/>\r\n");
      out.write("            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>   \r\n");
      out.write("            TMurph A3sdert\r\n");
      out.write("            </center>\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("    </div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
