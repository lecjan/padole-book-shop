package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class LoggedOut_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<html>    \r\n");
      out.write("<head>\r\n");
      out.write("    <title>Book Store PaDoLe</title>\r\n");
      out.write("    <meta charset=\"UTF-8\">\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/Site.css\" type=\"text/css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/jquery.dataTables.css\" type=\"text/css\">\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery-1.11.1.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery.dataTables.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/PadoleScripts.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("    <div class=\"page\">\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"header\">\r\n");
      out.write("           \r\n");
      out.write("            <div class=\"title\">\r\n");
      out.write("                <h1>Book Store PADOLE</h1>                \r\n");
      out.write("            </div>\r\n");
      out.write("            <div id=\"login_section\">\r\n");
      out.write("            <div id=\"login_buttons\">\r\n");
      out.write("                \r\n");
      out.write("        \r\n");
      out.write("                <form id=\"login_buttons_form\" method=\"post\" name=\"form1\">\r\n");
      out.write("                    <input type=\"button\" value=\"Login\" onclick=\"loginDisplay('Login')\"/>\r\n");
      out.write("                    <input type=\"button\" value=\"Register\" onclick=\"loginDisplay('Register')\"/>                \r\n");
      out.write("                </form>                \r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("            \r\n");
      out.write("            <!-- menu container -->\r\n");
      out.write("            <div class=\"clear hideSkiplink\"> \r\n");
      out.write("                \r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("        </div>     \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"main\">\r\n");
      out.write("            <div class=\"main_left\">\r\n");
      out.write("                    Once upon the time there were three humble guys...\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    Paul, Don and Lech.\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    They didn't have time to read books no more, \r\n");
      out.write("                    so decided to sell them...\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    Didn't have shop nor money to hire website developer to design \r\n");
      out.write("                    <br/>\r\n");
      out.write("                    for them e-Commerce website, so...\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    They made a modest one - themselves....\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    <br/>\r\n");
      out.write("                    And.. This is it!\r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"main_right\">\r\n");
      out.write("                <h2>EPILOGUE</h2>\r\n");
      out.write("                <br/>\r\n");
      out.write("                <h1>\"You have LOGGED OUT from our book store..\"</h1>\r\n");
      out.write("                \r\n");
      out.write("            <div id=\"login_display\">\r\n");
      out.write("                <!-- GUYS put here action to servlet forwarding to main.html/jsp when login successful -->\r\n");
      out.write("\r\n");
      out.write("                <!-- login_form is universal and is used to validate username and password only\r\n");
      out.write("                     either for LOGIN or REGISTER -->\r\n");
      out.write("                <form id=\"login_form\" method=\"post\" name=\"form1\" action=\"ActionServlet\">\r\n");
      out.write("\r\n");
      out.write("                    <!-- USERNAME -->\r\n");
      out.write("                    <label>Username :</label>\r\n");
      out.write("                    <!-- pattern for username validation\r\n");
      out.write("                        // at least one number, one lowercase and one uppercase letter\r\n");
      out.write("                        // at least six characters\r\n");
      out.write("                    \r\n");
      out.write("                        // set custom Match format text\r\n");
      out.write("                    -->                \r\n");
      out.write("                    <input type=\"text\" required pattern=\"\\w+\" name=\"username\" id=\"usernameText\"/>\r\n");
      out.write("                    <br/><br/>\r\n");
      out.write("\r\n");
      out.write("                    <!-- PASSWORD -->\r\n");
      out.write("                    <label>Password :</label>\r\n");
      out.write("                    <!-- pattern for password validation\r\n");
      out.write("                        // at least one number, one lowercase and one uppercase letter\r\n");
      out.write("                        // at least six characters that are letters, numbers or the underscore\r\n");
      out.write("                    \r\n");
      out.write("                        // set custom Match format text                    \r\n");
      out.write("                    -->\r\n");
      out.write("                    <input type=\"password\" required pattern=\"(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,}\" name=\"password\" id=\"passwordText\" />\r\n");
      out.write("                    <br/><br/>\r\n");
      out.write("\r\n");
      out.write("                    <!-- BUTTONS -->\r\n");
      out.write("                    <!-- submit value changes according to value set by login_buttons_form,\r\n");
      out.write("                         and this is to use for login_form to display proper submit type,\r\n");
      out.write("                         also hidden input takes same value as an action to use by servlet -->\r\n");
      out.write("                    \r\n");
      out.write("                    <input type=\"hidden\" name=\"action\" value=\"notset\" id=\"submit_action\"/>\r\n");
      out.write("                    <input type=\"submit\" value=\"notset\" id=\"submit_login\" onclick=\"validateLogin()\"/>\r\n");
      out.write("                    <input type=\"button\" value=\"Cancel\" id=\"submit_cancel\" onclick=\"loginCancel()\"/>\r\n");
      out.write("                    <br/><br/>\r\n");
      out.write("                    \r\n");
      out.write("                    <!-- ERROR MESSAGE FOR JAVASCRIPT -->\r\n");
      out.write("                    <div id=\"error\"></div>\r\n");
      out.write("                    \r\n");
      out.write("                </form>\r\n");
      out.write("            </div>                \r\n");
      out.write("            </div>        \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->        \r\n");
      out.write("        <div class=\"footer\">   \r\n");
      out.write("            <center>\r\n");
      out.write("                <br/><br/><br/>\r\n");
      out.write("            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>   \r\n");
      out.write("            TMurph A3sdert\r\n");
      out.write("            </center>\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("    </div>\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
