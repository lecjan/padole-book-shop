package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.List;
import java.util.*;
import DTO.User;
import DTO.Book;
import java.util.ArrayList;

public final class listBooks_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <title>Book Store PaDoLe</title>\r\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/Site.css\" type=\"text/css\">\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"CSS/jquery.dataTables.css\" type=\"text/css\">\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery-1.11.1.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/jquery.dataTables.min.js\"></script>\r\n");
      out.write("    <script type=\"text/javascript\" src=\"JS/PadoleScripts.js\"></script>\r\n");
      out.write("</head>\r\n");
      out.write("\r\n");
      out.write("<body>\r\n");
      out.write("    <div class=\"page\">\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"header\">\r\n");
      out.write("           \r\n");
      out.write("            <div class=\"title\">\r\n");
      out.write("                <h1>Book Store PADOLE</h1>                \r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("            <div id=\"login_section\">\r\n");
      out.write("                <div id=\"login_buttons\">\r\n");
      out.write("                    <form id=\"login_logout_form\" method=\"post\" name=\"form3\" action='ActionServlet'>\r\n");
      out.write("                        <input type=\"hidden\" name=\"action\" value=\"Logout\" id=\"submit_action\"/>\r\n");
      out.write("                        <input type=\"Submit\" value=\"Logout\" onclick=\"\"/>                \r\n");
      out.write("                    </form>   \r\n");
      out.write("                </div>\r\n");
      out.write("            </div>            \r\n");
      out.write("            \r\n");
      out.write("            <!-- menu container -->\r\n");
      out.write("            <div class=\"clear hideSkiplink\">                 \r\n");
      out.write("            </div>\r\n");
      out.write("            \r\n");
      out.write("        </div>     \r\n");
      out.write("        <!-- ************************************************************* -->\r\n");
      out.write("        <div class=\"main\">\r\n");
      out.write("            <div class=\"main_left\">\r\n");
      out.write("\r\n");
      out.write("<table id=\"example\" class=\"display\" cellspacing=\"0\" width=\"100%\" style=\"font-size: .8em\">\r\n");
      out.write("        <thead>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <th>ID</th>\r\n");
      out.write("                <th>Title</th>\r\n");
      out.write("                <th>Author</th>\r\n");
      out.write("                <th>Genre</th>\r\n");
      out.write("                <th>Price</th>\r\n");
      out.write("                <th>Qty</th>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </thead>\r\n");
      out.write(" \r\n");
      out.write("        <tfoot>\r\n");
      out.write("            <tr>\r\n");
      out.write("                <th>ID</th>\r\n");
      out.write("                <th>Title</th>\r\n");
      out.write("                <th>Author</th>\r\n");
      out.write("                <th>Genre</th>\r\n");
      out.write("                <th>Price</th>\r\n");
      out.write("                <th>Qty</th>\r\n");
      out.write("            </tr>\r\n");
      out.write("        </tfoot>\r\n");
      out.write(" \r\n");
      out.write("        <tbody>            \r\n");
      out.write("            ");

                ArrayList<Book> bookList = new ArrayList<>();
                bookList = (ArrayList) request.getSession().getAttribute("books");
                for (Book b: bookList){ 
                    if (b.getQty() != 0)
                {
      out.write("\r\n");
      out.write("                 \r\n");
      out.write("                    <tr onclick=\"orderBook(");
      out.print(b.getBookId() );
      out.write(',');
      out.write('\'');
      out.print(b.getTitle());
      out.write("','");
      out.print(b.getAuthor());
      out.write("','");
      out.print(b.getGenre() );
      out.write('\'');
      out.write(',');
      out.print(b.getPrice() );
      out.write(")\">\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getBookId() );
      out.write('"');
      out.write('>');
      out.print(b.getBookId() );
      out.write("</td>\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getTitle() );
      out.write('"');
      out.write('>');
      out.print(b.getTitle());
      out.write("</td>\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getAuthor());
      out.write('"');
      out.write('>');
      out.print(b.getAuthor());
      out.write("</td>\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getGenre() );
      out.write('"');
      out.write('>');
      out.print(b.getGenre() );
      out.write("</td>\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getPrice() );
      out.write('"');
      out.write('>');
      out.print(b.getPrice() );
      out.write("</td>\r\n");
      out.write("                        <td data-search=\"");
      out.print(b.getQty() );
      out.write('"');
      out.write('>');
      out.print(b.getQty() );
      out.write("</td>\r\n");
      out.write("                    </tr>                    \r\n");
      out.write("                ");
 }
                } 
      out.write("\r\n");
      out.write("        </tbody>\r\n");
      out.write("    </table>              \r\n");
      out.write("                <script type=\"text/javascript\">\r\n");
      out.write("                    $(document).ready(function() {\r\n");
      out.write("                        $('#example').dataTable();\r\n");
      out.write("                    } );\r\n");
      out.write("                </script>                    \r\n");
      out.write("                    \r\n");
      out.write("            </div>\r\n");
      out.write("\r\n");
      out.write("            <div class=\"main_right\">\r\n");
      out.write("                 <h2>Navigation</h2>\r\n");
      out.write("                 <h1>Where would you like to go.....</h1>\r\n");
      out.write("                <br>\r\n");
      out.write("                <div><a href=\"userAccount.jsp\">View/Edit Your Account Details</a></div>\r\n");
      out.write("                \r\n");
      out.write("                <br>\r\n");
      out.write("                <div><a href=\"ActionServlet?action=viewOrders\">View Your Orders</a></div>\r\n");
      out.write("                \r\n");
      out.write("                <h2>Chapter Three</h2>\r\n");
      out.write("                <h1>\"Select The One Book You Want..\"</h1>  \r\n");
      out.write("              \r\n");
      out.write("               \r\n");
      out.write("               <div id=\"single_order\" style=\"text-align: right\">\r\n");
      out.write("                    <form id=\"single_order_form\" method=\"post\" name=\"form5\" action=\"ActionServlet\">\r\n");
      out.write("                        <div id=\"order_Text\">\r\n");
      out.write("                            ID:<input type=\"text\" name=\"bookId\" value=\"a\" id=\"bookIdText\"readonly/><br/>\r\n");
      out.write("                            Title:<input type=\"text\"name=\"title\" value=\"b\" id=\"titleText\"readonly/><br/> \r\n");
      out.write("                            Author:<input type=\"text\" name=\"author\" value=\"c\" id=\"authorText\"readonly/><br/> \r\n");
      out.write("                            Genre:<input type=\"text\" name=\"genre\" value=\"d\" id=\"genreText\"readonly/><br/> \r\n");
      out.write("                            Price:<input type=\"text\" name=\"price\" value=\"e\" id=\"priceText\"readonly/><br/> \r\n");
      out.write("                        </div>\r\n");
      out.write("                        <input type=\"hidden\" name=\"action\" value=\"placeOrder\" id=\"submit_action\"/>\r\n");
      out.write("                        <input type=\"Submit\" value=\"Buy\" onclick=\"\"/> \r\n");
      out.write("                        <input type=\"button\" value=\"Cancel\" onclick=\"orderBookCancel()\"/> \r\n");
      out.write("                    </form>\r\n");
      out.write("                    \r\n");
      out.write("                </div>\r\n");
      out.write("            </div>        \r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("        <!-- ************************************************************* -->        \r\n");
      out.write("        <div class=\"footer\">   \r\n");
      out.write("            <center>\r\n");
      out.write("                <br/><br/><br/>\r\n");
      out.write("            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>            \r\n");
      out.write("            </center>\r\n");
      out.write("        </div>\r\n");
      out.write("        \r\n");
      out.write("    </div>\r\n");
      out.write("</body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
