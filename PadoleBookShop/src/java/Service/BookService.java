
package Service;

import DAO.BookDao;
import DTO.Book;
import Exception.DaoException;
import Servlet.ActionServlet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BookService {

    public Book selectBook(int id) {

        BookDao bd = new BookDao();

        Book b = null;

        try {
            b = bd.selectBook(id);
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return b;
    }

    public int sellBook(Book b) {
        int rows = -1;
        BookDao bd = new BookDao();
        try {
            rows = bd.sellBook(b.getBookId(), b.getQty());
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    public ArrayList<Book> getBooks() {
        BookDao bookDao = new BookDao();
        ArrayList<Book> booklist = new ArrayList<Book>();

        try {
            booklist = bookDao.viewAllBooks();
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return booklist;
    }
}
