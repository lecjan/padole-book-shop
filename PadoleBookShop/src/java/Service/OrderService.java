/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import DAO.OrderDao;
import DTO.Book;
import DTO.Order;
import DTO.User;
import Exception.DaoException;
import Servlet.ActionServlet;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;


public class OrderService {

    public int createOrder(Book b, User u) {
        int userId = u.getUserId();
        int bookId = b.getBookId();
        String title = b.getTitle();
        Date orderDate = new Date();

        Order o = new Order();
        OrderDao od = new OrderDao();

        o.setBookId(bookId);
        o.setBookTitle(title);
        o.setOrderDate(orderDate);
        o.setUserId(userId);

        int rows = -1;

        try {
            rows = od.placeOrder(o);
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return rows;
    }

    public ArrayList<Order> getOrders(User u) {
        OrderDao od = new OrderDao();
        ArrayList<Order> orderList = new ArrayList<Order>();

        int userId = u.getUserId();

        try {
            orderList = od.selectOrders(userId);
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return orderList;

    }
}
