/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import DAO.UserDao;
import DTO.User;
import Exception.DaoException;
import Servlet.ActionServlet;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserService {

    public User login(String username, String password) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");

            md.update(password.getBytes());
            byte[] bytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            generatedPassword = sb.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        User u = null;
        UserDao ud = new UserDao();
        try {
            u = ud.userLogIn(username, generatedPassword);
        } catch (DaoException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return u;
    }

    public User checkUser(String username) {
        User usr = null;
        UserDao usrDB = new UserDao();
        try {
            usr = usrDB.usernameCheck(username);
        } catch (DaoException e) {
            System.err.println("doPost from UserDao:" + e.getMessage());
        }
        return usr;
    }

    public int addUser(String username, String password, String firstName, String lastName, String phone, String address1, String address2, String city, String country) {
        int rows = -1;

        if (username != null && password != null && firstName != null && lastName != null && phone != null && address1 != null && address2 != null && city != null && country != null && !username.equals("") && !password.equals("")) {
            String generatedPassword = null;
            try {
                MessageDigest md = MessageDigest.getInstance("MD5");

                md.update(password.getBytes());
                byte[] bytes = md.digest();

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.length; i++) {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }

                generatedPassword = sb.toString();

            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            User u = new User();
            UserDao ud = new UserDao();

            u.setUsername(username);
            u.setPassword(generatedPassword);
            u.setfName(firstName);
            u.setsName(lastName);
            u.setPhone(phone);
            u.setAddress1(address1);
            u.setAddress2(address2);
            u.setCity(city);
            u.setCountry(country);

            try {
                rows = ud.addUser(u);
            } catch (DaoException ex) {
                Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rows;
    }

    public int editUser(User u, String firstName, String lastName, String phone, String address1, String address2, String city, String country) {
        int rows = -1;
        if (firstName != null && lastName != null && phone != null && address1 != null && address2 != null && city != null && country != null) {

            UserDao ud = new UserDao();

            u.setfName(firstName);
            u.setsName(lastName);
            u.setPhone(phone);
            u.setAddress1(address1);
            u.setAddress2(address2);
            u.setCity(city);
            u.setCountry(country);

            try {
                rows = ud.editUser(u);
            } catch (DaoException ex) {
                Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return rows;
    }

}
