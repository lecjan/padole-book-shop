package DAO;

import DTO.Book;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BookDao extends DAO{
    
    public ArrayList<Book> viewAllBooks() throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Book> books = new ArrayList<Book>();
        try {
            con = getConnection();

            String query = "SELECT * FROM books";
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                int bookId = rs.getInt("bookid");
                String title = rs.getString("title");
                String author = rs.getString("author");
                String genre = rs.getString("genre");
                double price = rs.getDouble("price");
                int qty = rs.getInt("qty");

                Book b = new Book(bookId, title, author, genre, price, qty);
                books.add(b);
            }
        } catch (SQLException e) {
            throw new DaoException("viewAllBooks(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("viewAllBooks(): " + e.getMessage());
            }
        }
        return books;		
    }
    
    public Book selectBook(int bookId) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Book b = null;
        try {
            con = getConnection();

            String query = "SELECT * FROM books WHERE bookId = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, bookId);

            rs = ps.executeQuery();

            if (rs.next()) {
                int Id = rs.getInt("bookid");
                String bookTitle = rs.getString("title");
                String author = rs.getString("author");
                String genre = rs.getString("genre");
                double price = rs.getDouble("price");
                int qty = rs.getInt("qty");

                b = new Book(Id, bookTitle, author, genre, price, qty);
                
            }
        } catch (SQLException e) {
            throw new DaoException("selectBook(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("selectBook(): " + e.getMessage());
            }
        }
        return b;		
    }
    
    public int sellBook(int bookId, int qty) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
            
            String query = "UPDATE books SET qty = ? WHERE bookid = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, (qty - 1));
            ps.setInt(2, bookId);
            

            rowsAffected = ps.executeUpdate();
            
        } catch (SQLException e) {
            throw new DaoException("editUser() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("editUser(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
    
}
