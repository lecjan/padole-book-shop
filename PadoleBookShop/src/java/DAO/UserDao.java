package DAO;

import DTO.User;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao extends DAO{
    
public User usernameCheck(String userName) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        try {
            con = getConnection();

            String query = "SELECT * FROM users WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, userName);

            rs = ps.executeQuery();

            if (rs.next()) {
                int userId = rs.getInt("userId");
                String username = rs.getString("username");
                String userPassword = rs.getString("password");
                String fName = rs.getString("fName");
                String sName = rs.getString("sName");
                String phone = rs.getString("phone");
                String address1 = rs.getString("addressLine1");
                String address2 = rs.getString("addressLine2");
                String city = rs.getString("city");
                String country = rs.getString("country");
                
                u = new User(userId, username, userPassword, fName, sName, phone, address1, address2, city, country);
            }

        } catch (SQLException e) {
            throw new DaoException("usernameCheck(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("usernameCheck(): " + e.getMessage());
            }
        }
        return u;
    }    
    
    public User userLogIn(String userName, String password) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        User u = null;

        try {
            con = getConnection();

            String query = "SELECT * FROM users WHERE username = ? AND password = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, userName);
            ps.setString(2, password);

            rs = ps.executeQuery();

            if (rs.next()) {
                int userId = rs.getInt("userId");
                String username = rs.getString("username");
                String userPassword = rs.getString("password");
                String fName = rs.getString("fName");
                String sName = rs.getString("sName");
                String phone = rs.getString("phone");
                String address1 = rs.getString("addressLine1");
                String address2 = rs.getString("addressLine2");
                String city = rs.getString("city");
                String country = rs.getString("country");
                
                u = new User(userId, username, userPassword, fName, sName, phone, address1, address2, city, country);
            }

        } catch (SQLException e) {
            throw new DaoException("userLogIn(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("userLogIn(): " + e.getMessage());
            }
        }
        return u;
    }
    
    public int addUser(User u) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        int maxId = 0;
        try {
            con = getConnection();
            
            String query = "SELECT username FROM users WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, u.getUsername());

            rs = ps.executeQuery();
            if (rs.next()) {
                throw new DaoException("Username " + u.getUsername() + " already exists");
            }
            
            query = "SELECT MAX(userId) FROM users";
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            
            if (rs.next()) {
                maxId = rs.getInt("MAX(userId)");
            }
            
            String command = "INSERT INTO users (userId, username, password, fname, sname, phone, addressLine1, addressLine2, city, country) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            ps = con.prepareStatement(command);
            ps.setInt(1, maxId + 1);
            ps.setString(2, u.getUsername());
            ps.setString(3, u.getPassword());
            ps.setString(4, u.getfName());
            ps.setString(5, u.getsName());
            ps.setString(6, u.getPhone());
            ps.setString(7, u.getAddress1());
            ps.setString(8, u.getAddress2());
            ps.setString(9, u.getCity());
            ps.setString(10, u.getCountry());

            rowsAffected = ps.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("addUser() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("addUser(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
    
    public int editUser(User u) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        try {
            con = getConnection();
            
            String query = "UPDATE users SET fname = ?, sname = ?, phone = ?, addressLine1 = ?, addressLine2 = ?, city = ?, country = ? WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, u.getfName());
            ps.setString(2, u.getsName());
            ps.setString(3, u.getPhone());
            ps.setString(4, u.getAddress1());
            ps.setString(5, u.getAddress2());
            ps.setString(6, u.getCity());
            ps.setString(7, u.getCountry());
            ps.setString(8, u.getUsername());

            rowsAffected = ps.executeUpdate();
            
        } catch (SQLException e) {
            throw new DaoException("editUser() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("editUser(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
}
