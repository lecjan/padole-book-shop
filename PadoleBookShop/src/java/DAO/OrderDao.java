package DAO;

import DTO.Order;
import Exception.DaoException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OrderDao extends DAO{
    public int placeOrder(Order o) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int rowsAffected = 0;
        int maxId = 0;
        try {
            con = getConnection();
            
            String query = "SELECT MAX(orderId) FROM orders";
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            
            if (rs.next()) {
                maxId = rs.getInt("MAX(orderId)");
            }
            
            String command = "INSERT INTO orders (orderId, userId, bookId, bookTitle, orderdate) VALUES(?, ?, ?, ?, ?)";
            ps = con.prepareStatement(command);
            ps.setInt(1, maxId + 1);
            ps.setInt(2, o.getUserId());
            ps.setInt(3, o.getBookId());
            ps.setString(4, o.getBookTitle());
            ps.setDate(5, new java.sql.Date(o.getOrderDate().getTime()));

            rowsAffected = ps.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException("placeOrder() " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("placeOrder(): " + e.getMessage());
            }
        }
        return rowsAffected;
    }
    
    public ArrayList<Order> selectOrders(int userId) throws DaoException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Order> orders = new ArrayList<Order>();
        try {
            con = getConnection();

            String query = "SELECT * FROM orders WHERE userId = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, userId);

            rs = ps.executeQuery();

            while (rs.next()) {
                int orderId = rs.getInt("orderId");
                int orderUserId = rs.getInt("userId");
                int bookId = rs.getInt("bookId");
                String bookTitle = rs.getString("bookTitle");
                Date date = rs.getDate("orderdate");

                Order o = new Order(orderId, orderUserId, bookId, bookTitle, date);
                orders.add(o);
            }
        } catch (SQLException e) {
            throw new DaoException("selectOrders(): " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DaoException("selectOrders(): " + e.getMessage());
            }
        }
        return orders;		
    }    
}
