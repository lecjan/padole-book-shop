package DTO;

import java.util.Date;
import java.util.Objects;

public class Order {
    private int orderId;
    private int userId;
    private int bookId;
    private String bookTitle;
    private Date orderDate;

    public Order() {
    }

    public Order(int userId, int bookId, String bookTitle, Date orderDate) {
        this.userId = userId;
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.orderDate = orderDate;
    }

    public Order(int orderId, int userId, int bookId, String bookTitle, Date orderDate) {
        this.orderId = orderId;
        this.userId = userId;
        this.bookId = bookId;
        this.bookTitle = bookTitle;
        this.orderDate = orderDate;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    

    public int getOrderId() {
        return orderId;
    }

    public int getUserId() {
        return userId;
    }

    public int getBookId() {
        return bookId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + this.orderId;
        hash = 47 * hash + this.userId;
        hash = 47 * hash + this.bookId;
        hash = 47 * hash + Objects.hashCode(this.orderDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        if (this.bookId != other.bookId) {
            return false;
        }
        if (!Objects.equals(this.orderDate, other.orderDate)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Order{" + "orderId=" + orderId + ", userId=" + userId + ", bookId=" + bookId + ", orderDate=" + orderDate + '}';
    }
    
    
}
