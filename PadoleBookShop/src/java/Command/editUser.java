/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Command;

import DTO.User;
import Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author WIN7
 */
public class editUser implements command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;
        HttpSession session = request.getSession();
        
        String clientSessionId = session.getId();
        
        String storedSessionId = (String) session.getAttribute("loggedSessionId");
        
        if(clientSessionId.equals(storedSessionId)) {
            
            
        
            String firstName = request.getParameter("efName");
            String lastName = request.getParameter("esName");
            String phone = request.getParameter("ephone");
            String address1 = request.getParameter("eaddress1");
            String address2 = request.getParameter("eaddress2");
            String city = request.getParameter("ecity");
            String country = request.getParameter("ecountry");
            
            User u = (User) session.getAttribute("user");
        
            UserService us = new UserService();
            int rows = us.editUser(u, firstName, lastName, phone, address1, address2, city, country);
        
        
            if (rows != -1) {
                forwardToJsp = "/userAccount.jsp";
            } else {
                forwardToJsp = "/error.jsp";
            }
        } else {
            forwardToJsp = "/error.jsp";
        }
       
        return forwardToJsp;
    }
    
}
