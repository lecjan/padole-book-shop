
package Command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class gotoHomePage implements command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;
        
        HttpSession session = request.getSession();
        session.invalidate();
        
        forwardToJsp = "/main.html";
        
        return forwardToJsp;
    }
}
