
package Command;

import DTO.Order;
import DTO.User;
import Service.OrderService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class viewOrders implements command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        HttpSession session = request.getSession();

        String clientSessionId = session.getId();

        String storedSessionId = (String) session.getAttribute("loggedSessionId");

        if (clientSessionId.equals(storedSessionId)) {
            OrderService os = new OrderService();
            User u = (User) session.getAttribute("user");

            if (u != null) {
                ArrayList<Order> orderList = os.getOrders(u);

                session.setAttribute("orders", orderList);
                forwardToJsp = "/orders.jsp";
            } else {
                forwardToJsp = "/error.jsp";
            }
        } else {
            forwardToJsp = "/LoggedOut.jsp";
        }
        return forwardToJsp;
    }
}
