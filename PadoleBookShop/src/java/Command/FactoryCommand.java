package Command;

public class FactoryCommand {
   public command createCommand(String commandStr) 
    {
    	command command = null;
	//Instantiate the required Command object...
    	if (commandStr.equals("Login")) {
    		command = new Login();
    	}
        if (commandStr.equals("Register")) {
    		command = new Register();
    	}
    	if (commandStr.equals("Logout")) {
    		command = new Logout();
    	} 
        if (commandStr.equals("addUser")) {
                command = new addUser();
        }
        if (commandStr.equals("editUser")) {
                command = new editUser();
        }
        if (commandStr.equals("viewBooks")) {
                command = new viewBooks();
        }
        if (commandStr.equals("placeOrder")) {
                command = new placeOrder();
        }
        if (commandStr.equals("viewOrders")) {
                command = new viewOrders();
        }
        
        if (commandStr.equals("notset")) {
            command = new gotoHomePage();
        }
    	
    	return command;
    }     
}
