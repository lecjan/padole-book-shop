
package Command;

import DTO.User;
import Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class addUser implements command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("newusername");
        String password = (String) session.getAttribute("newuserpass");
        String firstName = request.getParameter("fName");
        String lastName = request.getParameter("sName");
        String phone = request.getParameter("phone");
        String address1 = request.getParameter("address1");
        String address2 = request.getParameter("address2");
        String city = request.getParameter("city");
        String country = request.getParameter("country");
        
        
        UserService us = new UserService();
        int rows = us.addUser(username, password, firstName, lastName, phone, address1, address2, city, country);
        User u = us.login(username, password);
        
            
            if (rows != -1 && u != null) {
                session = request.getSession();
                String clientSessionId = session.getId();
                session.setAttribute("loggedSessionId", clientSessionId);
                session.setAttribute("user", u);                
                              
                forwardToJsp = "/userAccount.jsp";
            } else {
                forwardToJsp = "/error.jsp";
            }
        return forwardToJsp;
    }
}
