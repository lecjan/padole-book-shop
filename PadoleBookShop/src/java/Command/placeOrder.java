
package Command;

import DTO.Book;
import DTO.User;
import Service.BookService;
import Service.OrderService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class placeOrder implements command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";

        HttpSession session = request.getSession();

        String clientSessionId = session.getId();

        String storedSessionId = (String) session.getAttribute("loggedSessionId");

        if (clientSessionId.equals(storedSessionId)) {
            String bookId = request.getParameter("bookId");
            BookService bs = new BookService();
            if (bookId != null) {
                int id = Integer.parseInt(bookId);
                Book b = bs.selectBook(id);

                User u = (User) session.getAttribute("user");

                if (u != null && b != null) {
                    OrderService os = new OrderService();
                    int rows = os.createOrder(b, u);
                    

                    if (rows != -1) {
                        int sellRows = bs.sellBook(b);
                        if (sellRows != -1)
                        {
                            forwardToJsp = "/orderComplete.jsp";
                        }
                        } else {
                        forwardToJsp = "/error.jsp";
                    }
                } else {
                    forwardToJsp = "/error.jsp";
                }
            } else {
                forwardToJsp = "/error.jsp";
            }

        } else {
            forwardToJsp = "/LoggedOut.jsp";
        }
        return forwardToJsp;

    }
}
