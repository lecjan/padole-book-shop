

package Command;



import DTO.User;
import Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Login implements command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        String forwardToJsp;
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (username != null && password != null)
        {
            UserService us = new UserService();
            User userLoggingIn = us.login(username, password);
            
            if (userLoggingIn != null)
            {
                HttpSession session = request.getSession();
                String clientSessionId = session.getId();
                session.setAttribute("loggedSessionId", clientSessionId);
                session.setAttribute("user", userLoggingIn);
                
                forwardToJsp = "/userAccount.jsp";				
            }
            else
            {
                forwardToJsp = "/loginFailure.jsp";	
            }
        }
        else 
        {
            forwardToJsp = "/loginFailure.jsp";	
        }
        
        return forwardToJsp;
    }
}
