
package Command;

import DTO.User;
import Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Register implements command{
     @Override
     public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "";
        
        //The user wants to register in...
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        UserService us = new UserService();
        User usr = us.checkUser(username);

       

        if (username != null && !username.equals("") ) {
            if (usr != null && username.equals(usr.getUsername())) {
                System.out.println("username entered: " + username);
                System.out.println("user Object  : " + usr.toString());
                System.out.println("account found  : " + usr.getUsername() + "," + usr.getPassword() + ";");
                forwardToJsp = "/loginRegisterUsed.jsp";
            } else {
                HttpSession session = request.getSession();
                String clientSessionId = session.getId();
                session.setAttribute("loggedSessionId", clientSessionId);
                session.setAttribute("newusername", username);
                session.setAttribute("newuserpass", password);
                System.out.println("username not found:"+session.getAttribute("newusername"));
                forwardToJsp = "/loginRegistration.jsp";
            }
        }
        return forwardToJsp;
    }
}
