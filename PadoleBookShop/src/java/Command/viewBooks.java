
package Command;

import DTO.Book;
import Service.BookService;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class viewBooks implements command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;

        HttpSession session = request.getSession();

        String clientSessionId = session.getId();

        String storedSessionId = (String) session.getAttribute("loggedSessionId");

        if (clientSessionId.equals(storedSessionId)) {
            BookService bs = new BookService();
            ArrayList<Book> booklist = bs.getBooks();

            session.setAttribute("books", booklist);
            forwardToJsp = "/listBooks.jsp";
        } else {
            forwardToJsp = "/LoggedOut.jsp";
        }

        return forwardToJsp;
    }
}
