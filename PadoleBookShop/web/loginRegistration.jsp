<%@page import="DTO.User"%>
<html>    
<head>
    <title>Book Store PaDoLe | Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/Site.css" type="text/css">
    <link rel="stylesheet" href="CSS/jquery.dataTables.css" type="text/css">
    <script type="text/javascript" src="JS/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="JS/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="JS/PadoleScripts.js"></script>
</head>

<body>
    <div class="page">
        
        <!-- ************************************************************* -->
        <div class="header">
           
            <div class="title">
                <h1>Book Store PADOLE</h1>                
            </div>
            
            <div id="login_section">
                <div id="login_buttons">
                    Welcome... 
                    <% String username = (String) (request.getSession().getAttribute("newusername")); %>
                    <% out.print(username); %>  
                    <form id="login_logout_form" method="post" name="form3" action='ActionServlet'>
                        <input type="hidden" name="action" value="Logout" id="submit_action"/>
                        <input type="Submit" value="Logout" onclick=""/>                
                    </form>   
                </div>                
            </div>            
            
            <!-- menu container -->
            <div class="clear hideSkiplink"> 
            </div>
            
        </div>     
        <!-- ************************************************************* -->
        <div class="main">
            <div class="main_left">
            </div>

            <div class="main_right">
                <h2>Chapter Three</h2>
                <h1>"To claim your Key<br/>we need your details.."</h1>
                
                
            <div id="login_display">
                <form id="registration_form" method="post" name="form4" action="ActionServlet">
                    <br/>
                    <!-- fName -->
                    <label>First name :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" required pattern="^[a-zA-Z' ]{2,50}$" name="fName" id="fNameText" title='Minimum 2 characters (letters, single quote and space)'/>
                    <br/>
                    <!-- sName -->
                    <label>Second name :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" required pattern="^[a-zA-Z' ]{2,50}$" name="sName" id="sNameText" title='Minimum 2 characters (letters,single quote and space)'/>
                    <br/>
                    <!-- phone -->
                    <label>Phone :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="tel" required pattern="^[0-9]{6,50}$" name="phone" id="phoneText" title='Minimum 6 digits'/>
                    <br/>
                    <!-- address1 -->
                    <label>Address line 1 :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" required pattern="^[0-9a-zA-Z'., ]{3,50}$" name="address1" id="address1Text" title='Minimum 3 characters (letters, digits, single quote, dot and space)'/>
                    <br/>      
                    <!-- address2 -->
                    <label>Address line 2 :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" name="address2" id="address2Text" title='Minimum 3 characters (letters, digits, single quote, dot and space)'/>
                    <br/> 
                    <!-- city -->
                    <label>Address City :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" required pattern="^[a-zA-Z' ]{3,50}$" name="city" id="cityText" title='Minimum 2 characters (letters, single quote and space)'/>
                    <br/> 
                    <!-- country -->
                    <label>Address Country :</label>
                    <!-- pattern for validation
                    -->                
                    <input type="text" required pattern="^[a-zA-Z' ]{1,50}$" name="country" id="countryText" title='Minimum 2 characters (letters, single quote and space)'/>
                    <br/><br/>                       

                    <!-- BUTTONS -->
                    <!-- submit value changes according to value set by login_buttons_form,
                         and this is to use for login_form to display proper submit type,
                         also hidden input takes same value as an action to use by servlet -->
                    
                    <input type="hidden" name="action" value="addUser" id="submit_action"/>
                    <input type="submit" value="Save" id="submit_login" onclick="validateRegistration()"/>
                    <input type="button" value="Cancel" id="submit_cancel" onclick="registrationCancel()"/>
                    <br/><br/>
                    
                    <!-- ERROR MESSAGE FOR JAVASCRIPT -->
                    <div id="error"></div>
                    
                </form>
            </div> 
                <script>registrationDisplay();</script>
            </div>        
        </div>
        
        <!-- ************************************************************* -->        
        <div class="footer">   
            <center>
                <br/><br/><br/>
            <h3>Paul Don Lech ............................... December 2014, CA3 OOW Patterns</h3>   
            TMurph A3sdert
            </center>
        </div>
        
    </div>
</body>
</html>


