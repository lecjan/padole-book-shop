function loginDisplay(loginType) {
    console.log("in login display");                        
    document.getElementById("submit_login").value = loginType;
    document.getElementById("submit_action").value = loginType;
    document.getElementById("login_display").style.height = "60px";
    document.getElementById("login_display").style.visibility = "visible";
    document.getElementById("login_buttons").style.visibility = "collapse";
    document.getElementById("login_buttons").style.height = "0px";   
    console.log("login type set to:"+document.getElementById("submit_login").value);
}

function loginCancel() {
    console.log("in login cancel");
    document.getElementById("usernameText").value = "";
    document.getElementById("passwordText").value = "";
    document.getElementById("login_buttons").style.height = "60px";
    document.getElementById("login_buttons").style.visibility = "visible";                         
    document.getElementById("login_display").style.visibility = "collapse";
    document.getElementById("login_display").style.height = "0px";
}   

function registrationDisplay() {
    console.log("in registration display");                        
    document.getElementById("login_display").style.height = "200px";
    document.getElementById("login_display").style.visibility = "visible";
    document.getElementById("login_buttons").style.visibility = "visible";
    document.getElementById("login_buttons").style.height = "60px";   
}

function registrationCancel() {
    console.log("in registration cancel");
    document.getElementById("submit_action").value = "notset";
    document.getElementById("login_buttons").style.height = "60px";
    document.getElementById("login_buttons").style.visibility = "visible";                         
    document.getElementById("login_display").style.visibility = "collapse";
    document.getElementById("login_display").style.height = "0px";
}   

function orderBook(a,b,c,d,e) {
    console.log("in order book");
    document.getElementById("single_order").style.height = "200px";
    document.getElementById("single_order").style.visibility = "visible";    
    document.getElementById("bookIdText").value = a;
    document.getElementById("titleText").value = b;
    document.getElementById("authorText").value = c;
    document.getElementById("genreText").value = d;
    document.getElementById("priceText").value = e;
}   

function orderBookCancel() {
    console.log("in order book cancel");
    document.getElementById("bookIdText").value = "";
    document.getElementById("titleText").value = "";
    document.getElementById("authorText").value = "";
    document.getElementById("genreText").value = "";
    document.getElementById("priceText").value = "";
    document.getElementById("single_order").style.visibility = "collapse";
    document.getElementById("single_order").style.height = "0px";    
}   

// Javascript validation used if HTML5 validation not supported
function validateLogin()
{
        var user=document.getElementById("usernameText").value;
        user=user.trim();
        var pass=document.getElementById("passwordText").value;
        pass=pass.trim();                            
        if(user === null)
        {
                document.getElementById('error').innerHTML="Please Enter Username";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }

        if(pass === null)
        {
                document.getElementById('error').innerHTML="Please Enter Password";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }
        return true;                           
}     

function validateRegistration()
{
    // to be amended to suit registration - may not be used.....
        var user=document.getElementById("usernameText").value;
        user=user.trim();
        var pass=document.getElementById("passwordText").value;
        pass=pass.trim();                            
        if(user === null)
        {
                document.getElementById('error').innerHTML="Please Enter Username";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }

        if(pass === null)
        {
                document.getElementById('error').innerHTML="Please Enter Password";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }
        return true;                           
} 

function editDetails() {
    document.getElementById("fNameDetails").readOnly = false;
    document.getElementById("sNameDetails").readOnly = false;
    document.getElementById("phoneDetails").readOnly = false;
    document.getElementById("address1Details").readOnly = false;
    document.getElementById("address2Details").readOnly = false;
    document.getElementById("cityDetails").readOnly = false;
    document.getElementById("countryDetails").readOnly = false;
    document.getElementById("edit_details").style.visibility = "collapse";
    document.getElementById("save").style.visibility = "visible";
}



